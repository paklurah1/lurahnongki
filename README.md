# LurahNongki



## Getting started Readme Example


NOTE:

## Make sure gradle

plugins app

```
plugins {
    id 'com.android.application'
    id 'org.jetbrains.kotlin.android'
    id 'kotlin-parcelize'
    id 'kotlin-kapt'
}

```


dependencies app

```
dependencies {

    implementation 'androidx.core:core-ktx:1.7.0'
    implementation 'androidx.appcompat:appcompat:1.5.1'
    implementation 'com.google.android.material:material:1.7.0'
    implementation 'androidx.constraintlayout:constraintlayout:2.1.4'
    testImplementation 'junit:junit:4.13.2'
    androidTestImplementation 'androidx.test.ext:junit:1.1.3'
    androidTestImplementation 'androidx.test.espresso:espresso-core:3.4.0'

    //Room Library
    implementation "androidx.room:room-runtime:2.4.0-alpha03"
    annotationProcessor "androidx.room:room-compiler:2.4.0-alpha03"
    kapt 'androidx.room:room-compiler:2.4.0-alpha03'
}

```


gradle.properties

```
android.useAndroidX=true
android.enableJetifier=true

```

## 1 Create Activity

A. MainActivity with recyclerview 
B. AddActivity
C. EditActivity

## 2 Change the layout 

A. MainActivity 
B. AddActivity
C. EditActivity

## 3 create item student xml

## 4 create enitites 

Note : make sure use 

```
import kotlinx.parcelize.Parcelize
```


## 5 create data -> DAO:

DAO for :

1. select ALL
2. insert 
3. delete 
4. update

## 6 create data -> database:

## 7 create file adapter

A. on this file have action to Edit activity
B. on this file have dialog to confirm delete

note : error missing function fetchData ignored this cause will be at next stap

## 8 implement main acitivity logic

this logic for fetch data


## 9 implement add acitivity logic

## 10 implement edit acitivity logic

## try running and enjoy


try run, and see the refrence at this branch




