package id.alik.tutor_android_16.data.dao

import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import id.alik.tutor_android_16.entity.StudentEntities

@Dao
interface StudentDao {
    @Query("Select * from StudentEntities")
    fun getAllStudent(): List<StudentEntities>

    @Query("Select * from StudentEntities where email =:email")
    fun getStudentByEmail(email: String): List<StudentEntities>

    @Insert(onConflict = REPLACE)
    fun insertStudent(studentEntities: StudentEntities): Long

    @Update
    fun updateStudent(studentEntities: StudentEntities): Int

    @Delete
    fun deleteStudent(studentEntities: StudentEntities): Int
}