package id.alik.tutor_android_16.commons

import android.app.Activity
import android.content.Intent
import id.alik.tutor_android_16.feature.videoplayer.MainVideoPlayerOnlineActivity
import id.alik.tutor_android_16.feature.videoplayer.MyVideoPlayerActivity
import id.alik.tutor_android_16.presenter.menuexample.model.MenuFeature
import id.alik.tutor_android_16.presenter.recylerviewexample.RecyclerViewActivity
import id.alik.tutor_android_16.presenter.roomexample.main.RoomActivity
import id.alik.tutor_android_16.presenter.sharedprefenceexample.SharedPreferenceExampleActivity

object Utils {

    fun manuNavigateTo(activity: Activity, menuFeature: MenuFeature) {
        var intent = Intent()
        when (menuFeature.nameFeature) {
            Constanta.MENU_RECYCLER_VIEW -> {
                intent = Intent(activity, RecyclerViewActivity::class.java)
            }
            Constanta.MENU_SHARED_PREFERENCE -> {
                intent = Intent(activity, SharedPreferenceExampleActivity::class.java)
            }
            Constanta.MENU_ROOM -> {
                intent = Intent(activity, RoomActivity::class.java)
            }
            Constanta.MENU_PLAY_VIDEO -> {
                intent = Intent(activity, MyVideoPlayerActivity::class.java)
            }
            Constanta.MENU_PLAY_VIDEO_ONLINE -> {
                intent = Intent(activity, MainVideoPlayerOnlineActivity::class.java)
            }
        }
        activity.startActivity(intent)

    }
}