package id.alik.tutor_android_16.commons

object Constanta {

    const val MENU_RECYCLER_VIEW = "RecyclerView Example"
    const val MENU_SHARED_PREFERENCE = "SharedPreference Example"
    const val MENU_ROOM = "Room Example"
    const val MENU_PLAY_VIDEO = "Play Video Example"
    const val MENU_PLAY_VIDEO_ONLINE = "Play Video Online Example"

    /*list of key */
    const val KEY_INTENT_STUDENT = "student"

}