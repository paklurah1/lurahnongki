package id.alik.tutor_android_16.presenter.roomexample.main.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import id.alik.tutor_android_16.R
import id.alik.tutor_android_16.commons.Constanta.KEY_INTENT_STUDENT
import id.alik.tutor_android_16.data.database.StudentDatabase
import id.alik.tutor_android_16.databinding.ItemStudentBinding
import id.alik.tutor_android_16.entity.StudentEntities
import id.alik.tutor_android_16.presenter.roomexample.main.RoomActivity
import id.alik.tutor_android_16.presenter.roomexample.edit.RoomEditActivity
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async

class RoomExampleAdapter :
    RecyclerView.Adapter<RoomExampleAdapter.RoomExampleHolder>() {

    lateinit var listContact: List<StudentEntities>

    fun setData(listContact: List<StudentEntities>) {
        this.listContact = listContact
    }

    class RoomExampleHolder(private val binding: ItemStudentBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData(studentData: StudentEntities) {
            binding.apply {
                tvID.text = studentData.id.toString()
                tvNama.text = studentData.nama
                tvEmail.text = studentData.email
                val intentKeEditActivity = Intent(
                    root.context,
                    RoomEditActivity::class.java
                )

                ivEdit.setOnClickListener {
                    intentKeEditActivity.putExtra(KEY_INTENT_STUDENT, studentData)
                    root.context.startActivity(intentKeEditActivity)
                }
                ivDelete.setOnClickListener {
                    AlertDialog.Builder(it.context)
                        .setPositiveButton(binding.root.context.getString(R.string.text_alert_dialog_yes)) { p0, p1 ->
                            val mDb = StudentDatabase.getInstance(binding.root.context)

                            GlobalScope.async {
                                val result = mDb?.studentDao()?.deleteStudent(studentData)

                                (binding.root.context as RoomActivity).runOnUiThread {
                                    if (result != 0) {
                                        Toast.makeText(
                                            it.context,
                                            binding.root.context.getString(
                                                R.string.text_alert_deskrition_tempalte,
                                                studentData.nama,
                                                binding.root.context.getString(R.string.text_alert_success),
                                            ),
                                            Toast.LENGTH_LONG
                                        ).show()
                                    } else {
                                        Toast.makeText(
                                            it.context,
                                            binding.root.context.getString(
                                                R.string.text_alert_deskrition_tempalte,
                                                studentData.nama,
                                                binding.root.context.getString(R.string.text_alert_failed),
                                            ),
                                            Toast.LENGTH_LONG
                                        ).show()
                                    }
                                }

                                (binding.root.context as RoomActivity).fetchData()
                            }
                        }.setNegativeButton(
                            binding.root.context.getString(R.string.text_alert_dialog_no)
                        ) { p0, p1 ->
                            p0.dismiss()
                        }
                        .setMessage("Apakah Anda Yakin ingin menghapus data ${studentData.nama}")
                        .setTitle("Konfirmasi Hapus").create().show()

                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RoomExampleHolder {

        return RoomExampleHolder(ItemStudentBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(holder: RoomExampleHolder, position: Int) {
        holder.bindData(listContact[position])
    }

    override fun getItemCount(): Int = listContact.size
}