package id.alik.tutor_android_16.presenter.roomexample.edit

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import id.alik.tutor_android_16.commons.Constanta.KEY_INTENT_STUDENT
import id.alik.tutor_android_16.data.database.StudentDatabase
import id.alik.tutor_android_16.databinding.ActivityRoomEditBinding
import id.alik.tutor_android_16.entity.StudentEntities
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async

class RoomEditActivity : AppCompatActivity() {
    lateinit var binding: ActivityRoomEditBinding
    private var studentDb: StudentDatabase? = null
    private var dataStudent: StudentEntities? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRoomEditBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setUpDatabase()
        dataStudent = intent.getParcelableExtra(KEY_INTENT_STUDENT)
        setDetailData()
        setUpAction()
    }

    private fun setUpDatabase() {
        studentDb = StudentDatabase.getInstance(this)
    }

    private fun setDetailData() {
        binding.apply {
            dataStudent?.let { studentData ->
                etNamaStudent.setText(studentData.nama)
                etEmailStudent.setText(studentData.email)
            }
        }
    }

    private fun setUpAction() {
        binding.btnSave.setOnClickListener {
            onValidation()
        }
    }

    private fun onValidation() {
        val etName = binding.etNamaStudent.text.toString()
        val etEmail = binding.etEmailStudent.text.toString()
        when {
            etName.isNullOrEmpty() -> {
                Toast.makeText(this, "nama murid kosong", Toast.LENGTH_SHORT).show()
            }
            etEmail.isNullOrEmpty() -> {
                Toast.makeText(this, "email murid kosong", Toast.LENGTH_SHORT).show()
            }
            else -> {
                dataStudent?.let {
                    it.nama = etName
                    it.email = etEmail
                    updateData(it)
                }
            }
        }
    }

    private fun updateData(studentEntities: StudentEntities) {
        GlobalScope.async {
            val result = studentDb?.studentDao()?.updateStudent(studentEntities)
            runOnUiThread {
                if (result != 0) {
                    Toast.makeText(
                        this@RoomEditActivity,
                        "data siswa berhasil di update",
                        Toast.LENGTH_SHORT
                    ).show()
                    finish()
                } else {
                    Toast.makeText(
                        this@RoomEditActivity,
                        "data siswa gagal di update",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }
}