package id.alik.tutor_android_16.presenter.recylerviewexample

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import id.alik.tutor_android_16.databinding.ActivityRecyclerViewBinding
import id.alik.tutor_android_16.presenter.recylerviewexample.adapter.ContactAdapter
import id.alik.tutor_android_16.presenter.recylerviewexample.model.MyContact

class RecyclerViewActivity : AppCompatActivity() {
    //#1 bikin activity , jangan lupa binding
    lateinit var binding: ActivityRecyclerViewBinding
    lateinit var listContactData: ArrayList<MyContact>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRecyclerViewBinding.inflate(layoutInflater)
        setContentView(binding.root)
        /*#12 buat data dummy untuk di kelolah adapter*/
        setUpData()
        /*#13 setup adapter , dan atur jenis layout recylerview, lalu implement ke recyclerviewnya*/
        setUpAdapter()
    }

    private fun setUpData() {
        listContactData = arrayListOf(
            MyContact("Kucing Sakti", "0121212121"),
            MyContact("Kucing Salto", "1212121"),
            MyContact("Kucing Terbang", "11212121"),
            MyContact("Kucing Meriang", "3131313131"),
            MyContact("Kucing Sakti", "0121212121"),
            MyContact("Kucing Salto", "1212121"),
            MyContact("Kucing Terbang", "11212121"),
            MyContact("Kucing Meriang", "3131313131"),
            MyContact("Kucing tersakiti", "0121212121"),
            MyContact("Kucing galau", "1212121"),
            MyContact("Kucing Meong", "11212121"),
            MyContact("Kucing WKKW", "3131313131"),
        )
    }

    private fun setUpAdapter() {
        /*#14 inisilisasi adapter */
        val adapterContact = ContactAdapter()
        /*#15 set data dummy ke dalam adapter */
        adapterContact.setData(listContactData)
        /*#16 tentukan view dan orientasinya, disini juga bisa ubah urutan dari bawah keatas , atau sebaliknya */
        val linearLayout = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        /*#17 panggil id recyclerview widget , lalu implement, UI dan adapter */
        binding.rvData.apply {
            layoutManager = linearLayout
            adapter = adapterContact
        }
    }


}