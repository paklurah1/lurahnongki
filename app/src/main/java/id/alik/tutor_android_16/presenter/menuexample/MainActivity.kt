package id.alik.tutor_android_16.presenter.menuexample

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import id.alik.tutor_android_16.commons.Constanta.MENU_PLAY_VIDEO
import id.alik.tutor_android_16.commons.Constanta.MENU_PLAY_VIDEO_ONLINE
import id.alik.tutor_android_16.commons.Constanta.MENU_RECYCLER_VIEW
import id.alik.tutor_android_16.commons.Constanta.MENU_ROOM
import id.alik.tutor_android_16.commons.Constanta.MENU_SHARED_PREFERENCE
import id.alik.tutor_android_16.commons.Utils
import id.alik.tutor_android_16.databinding.ActivityMainBinding
import id.alik.tutor_android_16.presenter.menuexample.adapter.MenuAdapter
import id.alik.tutor_android_16.presenter.menuexample.model.MenuFeature

class MainActivity : AppCompatActivity(), MenuAdapter.OnMenuClickListener {
    lateinit var binding: ActivityMainBinding
    lateinit var listMenuData: ArrayList<MenuFeature>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setUpDataMenu()
        setUpAdapterMenu()
    }

    private fun setUpDataMenu() {
        listMenuData = arrayListOf(
            MenuFeature(MENU_RECYCLER_VIEW),
            MenuFeature(MENU_SHARED_PREFERENCE),
            MenuFeature(MENU_ROOM),
            MenuFeature(MENU_PLAY_VIDEO),
            MenuFeature(MENU_PLAY_VIDEO_ONLINE)
        )
    }

    private fun setUpAdapterMenu() {
        binding.apply {
            val adapterMenu = MenuAdapter(this@MainActivity)
            /*#15 set data dummy ke dalam adapter */
            adapterMenu.setData(listMenuData)
            val linearLayout =
                LinearLayoutManager(this@MainActivity, LinearLayoutManager.VERTICAL, false)
            binding.rvMenu.apply {
                layoutManager = linearLayout
                adapter = adapterMenu
            }
        }
    }

    override fun onClickMenu(menuFeature: MenuFeature) = Utils.manuNavigateTo(this, menuFeature)
}