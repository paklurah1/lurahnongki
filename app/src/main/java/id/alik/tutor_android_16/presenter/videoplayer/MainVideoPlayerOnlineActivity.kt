package id.alik.tutor_android_16.feature.videoplayer

import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.webkit.URLUtil
import android.widget.MediaController
import android.widget.Toast
import android.widget.VideoView
import androidx.appcompat.app.AppCompatActivity
import id.alik.tutor_android_16.databinding.ActivityMainVideoPlayerOnlineBinding

class MainVideoPlayerOnlineActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainVideoPlayerOnlineBinding

    //URL Video from Internet
    private val VIDEO_SAMPLE = "https://storage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"

    // Current playback position (in milliseconds).
    private var mCurrentPosition = 0

    // Tag for the instance state bundle.
    private val PLAYBACK_TIME = "play_time"

    /*controler*/
    private lateinit var mediaController: MediaController
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainVideoPlayerOnlineBinding.inflate(layoutInflater)
        setContentView(binding.root)
        if (savedInstanceState != null) {
            mCurrentPosition = savedInstanceState.getInt(PLAYBACK_TIME);
        }
        setUpMediaController()
    }

    override fun onPause() {
        super.onPause()
        // In Android versions less than N (7.0, API 24), onPause() is the
        // end of the visual lifecycle of the app.  Pausing the video here
        // prevents the sound from continuing to play even after the app
        // disappears.
        //
        // This is not a problem for more recent versions of Android because
        // onStop() is now the end of the visual lifecycle, and that is where
        // most of the app teardown should take place.
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            binding.videoViewOnline.pause()
        }
    }

    override fun onStart() {
        super.onStart()
        initializePlayer()
    }

    override fun onStop() {
        super.onStop()
        binding.videoViewOnline.stopPlayback()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        // Save the current playback position (in milliseconds) to the
        // instance state bundle.
        outState.putInt(PLAYBACK_TIME, binding.videoViewOnline.currentPosition)
    }

    private fun setUpMediaController() {
        mediaController = MediaController(this)
        mediaController.setMediaPlayer(binding.videoViewOnline)
        binding.videoViewOnline.setMediaController(mediaController)
    }

    private fun initializePlayer() {
        binding.apply {
            tvBuffering.visibility = VideoView.VISIBLE
            // Buffer and decode the video sample.
            val videoUri: Uri = getMedia(VIDEO_SAMPLE)
            videoViewOnline.setVideoURI(videoUri)
            // Listener for onPrepared() event (runs after the media is prepared).
            videoViewOnline.setOnPreparedListener {
                tvBuffering.visibility = VideoView.INVISIBLE
                // Restore saved position, if available.
                if (mCurrentPosition > 0) {
                    videoViewOnline.seekTo(mCurrentPosition)
                } else {
                    // Skipping to 1 shows the first frame of the video.
                    videoViewOnline.seekTo(1)
                }
                /*start playing*/
                videoViewOnline.start()
            }

            // Listener for onCompletion() event (runs after media has finished playing).
            videoViewOnline.setOnCompletionListener {
                Toast.makeText(
                    this@MainVideoPlayerOnlineActivity, "Video Playback Completed",
                    Toast.LENGTH_SHORT
                ).show()

                // Return the video position to the start.
                videoViewOnline.seekTo(0)
            }
        }
    }

    // Get a Uri for the media sample regardless of whether that sample is
    // embedded in the app resources or available on the internet.
    private fun getMedia(mediaName: String): Uri {
        return if (URLUtil.isValidUrl(mediaName)) {
            // Media name is an external URL.
            Uri.parse(mediaName)
        } else {

            // you can also put a video file in raw package and get file from there as shown below
            Uri.parse(
                "android.resource://" + packageName +
                        "/raw/" + mediaName
            )
        }
    }

}