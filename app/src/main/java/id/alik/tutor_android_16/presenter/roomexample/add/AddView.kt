package id.alik.tutor_android_16.presenter.roomexample.add

interface AddView {
    fun showToast(message: String)
}