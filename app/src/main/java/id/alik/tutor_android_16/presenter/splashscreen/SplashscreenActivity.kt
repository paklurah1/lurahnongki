package id.alik.tutor_android_16.presenter.splashscreen

import android.content.Context
import android.content.Intent
import android.media.AudioAttributes
import android.media.AudioManager
import android.media.SoundPool
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import id.alik.tutor_android_16.R
import id.alik.tutor_android_16.databinding.ActivitySplashscreenBinding
import id.alik.tutor_android_16.presenter.menuexample.MainActivity

class SplashscreenActivity : AppCompatActivity() {
    lateinit var binding: ActivitySplashscreenBinding

    /*init variable*/
    private val MAX_STREAMS = 1
    private lateinit var soundPool: SoundPool
    private var soundId = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashscreenBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSoundPlay()
    }

    private fun setSoundPlay() {
        //kitkat 4.2 keatas
        if (Build.VERSION.SDK_INT >= 21) {
            val audioAttributes = AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_GAME)
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION).build()
            val builder = SoundPool.Builder()
            builder.setAudioAttributes(audioAttributes).setMaxStreams(MAX_STREAMS)
            this.soundPool = builder.build()
        } else {
            this.soundPool = SoundPool(MAX_STREAMS, AudioManager.STREAM_MUSIC, 0)
        }
        this.soundPool.setOnLoadCompleteListener { soundPool, i, i2 ->
            playBackSound()
        }
        this.soundId = this.soundPool.load(this, R.raw.app_src_main_res_raw_gun, 1)
    }

    private fun playBackSound() {
        val managerSystem = getSystemService(Context.AUDIO_SERVICE) as AudioManager
        val actualVolume = managerSystem.getStreamVolume(AudioManager.STREAM_MUSIC).toFloat()
        val maxVolume = managerSystem.getStreamMaxVolume(AudioManager.STREAM_MUSIC).toFloat()
        val volume = actualVolume / maxVolume
        this.soundPool.play(this.soundId, volume, volume, 1, 0, 1f)
        //uncoment this if want to play SFX more long
//        Handler(Looper.getMainLooper()).postDelayed({
//            startActivity(Intent(this, MainActivity::class.java))
//            finish()
//        }, 3000)
        //comment this if want to play SFX more long
        startActivity(Intent(this, MainActivity::class.java))
        finish()

    }
}