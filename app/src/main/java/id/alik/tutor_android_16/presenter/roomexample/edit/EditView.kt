package id.alik.tutor_android_16.presenter.roomexample.edit

interface EditView {
    fun showToast(message: String)
}