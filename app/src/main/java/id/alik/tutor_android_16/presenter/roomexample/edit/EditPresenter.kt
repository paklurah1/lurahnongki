package id.alik.tutor_android_16.presenter.roomexample.edit

interface EditPresenter {
    fun setUpDatabase()
}