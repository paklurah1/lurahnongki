package id.alik.tutor_android_16.presenter.sharedprefenceexample

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import id.alik.tutor_android_16.databinding.ActivitySharedPreferenceExampleBinding

class SharedPreferenceExampleActivity : AppCompatActivity() {
    lateinit var binding: ActivitySharedPreferenceExampleBinding
    /*Region untuk manage shared preference*/
    lateinit var sharedPreferences: SharedPreferences
    lateinit var editor: SharedPreferences.Editor

    /*end region*/
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySharedPreferenceExampleBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setUpShredPreference()
        setUpAction()
    }

    private fun setUpShredPreference() {
        sharedPreferences = this.getSharedPreferences(TABLE_DATA, Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()
    }

    private fun setUpAction() {
        /*untuk save data */
        binding.btnSave.setOnClickListener {
            val idData = binding.etInputId.text.toString().toInt()
            val nameData = binding.etInputName.text.toString()
            editor.putInt(KEY_ID, idData)
            editor.putString(KEY_NAME, nameData)
            editor.apply()
            Toast.makeText(this, "Save Berhasil", Toast.LENGTH_SHORT).show()
            clearEditText()
        }
        /*untuk view data*/
        binding.btnView.setOnClickListener {
            val dataIdView = sharedPreferences.getInt(KEY_ID, 0)
            val dataNameView = sharedPreferences.getString(KEY_NAME, "-")
            if (dataIdView != 0 && dataNameView != "-") {
                Toast.makeText(this, "Data ditampilkan", Toast.LENGTH_SHORT).show()
                binding.apply {
                    tvShowId.text = dataIdView.toString()
                    tvShowName.text = dataNameView
                }
            } else {
                Toast.makeText(this, "Data Kosong", Toast.LENGTH_SHORT).show()
            }
        }
        /*delete data*/
        binding.btnClear.setOnClickListener {
            editor.clear()
            editor.apply()
            clearEditText()
            clearView()
            Toast.makeText(this, "Data Berhasil di Hapus", Toast.LENGTH_SHORT).show()
        }
    }

    private fun clearEditText() {
        binding.apply {
            etInputId.text.clear()
            etInputName.text.clear()
        }
    }

    private fun clearView() {
        binding.apply {
            tvShowId.text = ""
            tvShowName.text = ""
        }
    }

    companion object {
        const val KEY_ID = "KEY_ID"
        const val KEY_NAME = "KEY_NAME"
        const val TABLE_DATA = "kotlinsharedpreference"
    }
}