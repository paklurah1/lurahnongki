package id.alik.tutor_android_16.presenter.roomexample.main

/*2 ini adalah jembatan untuk akses function di presenter dari activity*/
interface MainPresenter {
    fun setUpDatabase()
    fun fetchData()
}