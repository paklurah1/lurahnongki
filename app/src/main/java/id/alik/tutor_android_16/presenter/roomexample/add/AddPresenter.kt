package id.alik.tutor_android_16.presenter.roomexample.add

interface AddPresenter {
    fun setUpDatabase()
}