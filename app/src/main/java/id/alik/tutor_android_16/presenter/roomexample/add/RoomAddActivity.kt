package id.alik.tutor_android_16.presenter.roomexample.add

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import id.alik.tutor_android_16.data.database.StudentDatabase
import id.alik.tutor_android_16.databinding.ActivityRoomAddBinding
import id.alik.tutor_android_16.entity.StudentEntities
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async

class RoomAddActivity : AppCompatActivity() {
    lateinit var binding: ActivityRoomAddBinding
    private var studentDb: StudentDatabase? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRoomAddBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setUpDatabase()
        setUpAction()
    }

    private fun setUpDatabase() {
        studentDb = StudentDatabase.getInstance(this)
    }

    private fun setUpAction() {
        binding.btnSave.setOnClickListener {
            onValidation()
        }
    }

    private fun onValidation() {
        val etName = binding.etNamaStudent.text.toString()
        val etEmail = binding.etEmailStudent.text.toString()
        when {
            etName.isNullOrEmpty() -> {
                Toast.makeText(this, "nama murid kosong", Toast.LENGTH_SHORT).show()
            }
            etEmail.isNullOrEmpty() -> {
                Toast.makeText(this, "email murid kosong", Toast.LENGTH_SHORT).show()
            }
            else -> {
                val studentEntities = StudentEntities(
                    null,
                    nama = etName,
                    email = etEmail
                )
                saveData(studentEntities)
            }
        }
    }

    private fun saveData(studentEntities: StudentEntities) {
        GlobalScope.async {
            val result = studentDb?.studentDao()?.insertStudent(studentEntities)
            runOnUiThread {
                if (result != 0.toLong()) {
                    Toast.makeText(
                        this@RoomAddActivity,
                        "data siswa berhasil ditambahkan",
                        Toast.LENGTH_SHORT
                    ).show()
                    finish()
                } else {
                    Toast.makeText(
                        this@RoomAddActivity,
                        "data siswa gagal ditambahkan",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }
}