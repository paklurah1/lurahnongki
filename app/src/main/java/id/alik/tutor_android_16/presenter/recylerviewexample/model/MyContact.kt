package id.alik.tutor_android_16.presenter.recylerviewexample.model
/*#4 buat model data , penampung alias POJO class*/
data class MyContact(
    val nameContact: String, // Menampung data nama
    val noHp: String
)
