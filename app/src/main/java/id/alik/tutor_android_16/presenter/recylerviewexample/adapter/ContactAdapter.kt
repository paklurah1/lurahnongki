package id.alik.tutor_android_16.presenter.recylerviewexample.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.alik.tutor_android_16.databinding.ItemDataBinding
import id.alik.tutor_android_16.presenter.recylerviewexample.model.MyContact

/*#5 buat adapter untuk menjalankan recylerview*/
class ContactAdapter :
    RecyclerView.Adapter<ContactAdapter.ContactHolder>() {
    /*#7 implement override class adapternya*/
    lateinit var listContact: ArrayList<MyContact>
    /*#11  buat fungsi untuk set data */
    fun setData(listContact: ArrayList<MyContact>) {
        this.listContact = listContact
    }

    /*#6 buat class view holdernya*/
    class ContactHolder(private val binding: ItemDataBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData(listContact: MyContact) {
            /*#10 gunakan data per posisi view dan masukan data ke setiap text view/ view widgetnya*/
            binding.apply {
                tvName.text = listContact.nameContact
                tvNoHp.text = listContact.noHp
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactHolder {
        /*#8 implement binding untuk view (layout item binding ) holder nya*/
        return ContactHolder(ItemDataBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(holder: ContactHolder, position: Int) {
        /*#9 ikat data pada fungsi class holder dengan cara, panggil holder dan fungsi open pada view holder
        * dala ini fungsi opennya adalah bindData, lalu masukan data sesuai keperluan fungsinya
        * isi data sesuai posisinya*/
        holder.bindData(listContact[position])
    }

    override fun getItemCount(): Int = listContact.size
}