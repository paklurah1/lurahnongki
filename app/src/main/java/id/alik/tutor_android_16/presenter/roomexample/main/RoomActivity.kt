package id.alik.tutor_android_16.presenter.roomexample.main

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import id.alik.tutor_android_16.data.database.StudentDatabase
import id.alik.tutor_android_16.databinding.ActivityRoomBinding
import id.alik.tutor_android_16.entity.StudentEntities
import id.alik.tutor_android_16.presenter.roomexample.add.RoomAddActivity
import id.alik.tutor_android_16.presenter.roomexample.main.adapter.RoomExampleAdapter


/**6.A turunkan function dari interface Mainview **/
/**6.C implement function dari interface Mainview (override function) **/
class RoomActivity : AppCompatActivity(), MainView {
    lateinit var binding: ActivityRoomBinding

    /** 8.A hapus deklarasi datbase dan buat deklarasi presenter**/
    private var studentAdapter: RoomExampleAdapter? = null
    lateinit var presenter: MainRoomPresenter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRoomBinding.inflate(layoutInflater)
        setContentView(binding.root)
        /** 8.B hapus setup database **/
        /** 8.C buat setup presenter **/
        setUpPresenter()
        setUpAction()
        setUpAdapter()
        /** 10.A jika presenter sudah di deklarasi, panggil fungsi fetch data**/
        presenter.fetchData()

    }

    /** 11 buat fungsi open, untuk memanggil fungsi presenter**/
    fun fetchData() = presenter.fetchData()

    /** 9 buat setup presenter , lalu panggil presenter yang ingin digunakan**/
    private fun setUpPresenter() {
        presenter = MainRoomPresenter(mainView = this)
        /** 10 jika presenter sudah di deklarasi, panggil fungsi setup databsase**/
        presenter.setUpDatabase()
    }

    private fun setUpAction() {
        binding.fabAdd.setOnClickListener {
            val keActivityAdd = Intent(this@RoomActivity, RoomAddActivity::class.java)
            startActivity(keActivityAdd)
        }
    }

    private fun setUpAdapter() {
        studentAdapter = RoomExampleAdapter()
    }

    override fun onResume() {
        super.onResume()
        /** 10.A jika presenter sudah di deklarasi, panggil fungsi fetch data**/
        presenter.fetchData()
    }

    override fun onDestroy() {
        super.onDestroy()
        StudentDatabase.destroyInstance()
    }

    /** 7 pindahkan logic setListData ke function ovveride setListDataToView **/
    override fun setListDataToView(listStudent: List<StudentEntities>?) {
        if (listStudent?.isNotEmpty() == true) {
            runOnUiThread {
                val linearLayout =
                    LinearLayoutManager(this@RoomActivity, LinearLayoutManager.VERTICAL, false)
                binding.recyclerView.apply {
                    studentAdapter?.setData(listStudent)
                    layoutManager = linearLayout
                    adapter = studentAdapter
                }
            }
        } else {
            //set view kosong
        }
    }
}