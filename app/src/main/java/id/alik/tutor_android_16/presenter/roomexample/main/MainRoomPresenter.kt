package id.alik.tutor_android_16.presenter.roomexample.main

import android.content.Context
import id.alik.tutor_android_16.data.database.StudentDatabase
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

/*3 buat presenter impl atau kelas presenter untuk menjalankan logic interface presenternya*/
class MainRoomPresenter(private val mainView: MainView) :
    MainPresenter {
    /*4 buat variable untuk database disini*/
    lateinit var studentDb: StudentDatabase

    /**5 implement function dari interface **/
    /** A. function untuk setupdatabse **/
    override fun setUpDatabase() {
        studentDb = StudentDatabase.getInstance(mainView as Context)!!
    }

    /** B. function untuk ambil data dari database **/
    override fun fetchData() {
        GlobalScope.launch {
            val listStudent = studentDb.studentDao().getAllStudent()
            /** 6.B hubungkan hasil dari pengambilan data dari databse ke view / Acitivty/ fragment, dengan cara
             * memanggil interface/jembatan view
             * note kirimkan data yang sesuai dengan fungsi ex : data student **/
            mainView.setListDataToView(listStudent)
        }
    }
}