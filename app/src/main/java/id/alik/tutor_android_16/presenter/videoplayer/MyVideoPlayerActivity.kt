package id.alik.tutor_android_16.feature.videoplayer

import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import id.alik.tutor_android_16.R
import id.alik.tutor_android_16.databinding.ActivityMyVideoPlayerBinding

class MyVideoPlayerActivity : AppCompatActivity() {
    lateinit var binding: ActivityMyVideoPlayerBinding
    private var urlVideoRaw = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMyVideoPlayerBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setVideoProperty()
        setAction()

    }

    private fun setAction() {
        binding.apply {
            btnPlay.setOnClickListener {
                videoView.start()
            }
            videoView.setOnClickListener {
                videoView.start()
            }
            btnPause.setOnClickListener {
                videoView.pause()
            }
            btnStop.setOnClickListener {
                videoView.stopPlayback()
                videoView.setVideoURI(Uri.parse(urlVideoRaw))
            }
        }
    }

    private fun setVideoProperty() {
        urlVideoRaw = "android.resource://$packageName/${R.raw.example_video_footage}"
        binding.videoView.setVideoURI(Uri.parse(urlVideoRaw))
    }
}