package id.alik.tutor_android_16.presenter.roomexample.main

import id.alik.tutor_android_16.entity.StudentEntities

/*1 ini adalah jembatan untuk akses function di presenter dari activity*/
interface MainView {
    fun setListDataToView(listStudent: List<StudentEntities>?)
}